#!/bin/bash

foo=$@

curl -s "https://api.voicetext.jp/v1/tts" \
     -o "tts.wav" \
     -u "YOUR_API_KEY:" \
     -d "text=\"${foo}\"" \
     -d "speaker=hikari" && afplay tts.wav && rm tts.wav && $@