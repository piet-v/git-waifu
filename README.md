# Git Waifu

Automatically applies text-to-speech to git commands.

# Requirements

* Get a key from https://cloud.voicetext.jp/webapi/api_keys/new
* Z-shell

# Known issues
* Commands which contain single/double quotes will not execute properly after text-to-speech has concluded

# Installation

1) Copy tts.sh from this repo to ~/tts.sh
3) Replace YOUR_API_KEY with your key in ~/tts.sh
4) Add aliases from .zshrc in this repo to ~/.zshrc

# Config

* New aliases can be added to .zshrc
* Text-to-speech config options can be found on https://cloud.voicetext.jp/webapi/docs/introduction